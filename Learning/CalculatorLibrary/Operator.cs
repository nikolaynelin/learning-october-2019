﻿using System;

namespace CalculatorLibrary
{
    internal class Operator
    {
        public char Sign { get; }

        public int Priority { get; }

        public Func<double, double, double> Calculate { get; }

        public Action<double, double> PrintToConsole { get; }

        public Operator(char sign, int priority, Func<double, double, double> func, Action<double, double> printToConsole = null)
        {
            Sign = sign;
            Priority = priority;
            Calculate = func;
            PrintToConsole = printToConsole;
        }
    }
}
