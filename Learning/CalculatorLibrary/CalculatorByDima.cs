﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Calculator
{
    public static class CalculatorByDima
    {
        public static double Calculate(String a)
        {
            a = Regex.Replace(a, @"\s+", "");

            CheckForValidity(a);

            var result = OperationInBrackets(a);
            return Math.Round(double.Parse(result), 5, MidpointRounding.AwayFromZero);
        }
        public static void CheckForValidity(String a)
        {
            var countOpenBrackets = 0;
            var countCloseBrackets = 0;
            Regex regex = new Regex(@"[0-9\.,\*/\+^)(-]");
            
            for(int i = 0; i < a.Length; i++)
            {
                if(!regex.IsMatch(a[i].ToString())) throw new ArgumentException("Invalid characters entered");
                if (a[i] == '(')
                {
                    countOpenBrackets++;
                    if(i != 0 && a[i - 1] == ')' && a[i] == '(') throw new ArgumentException("Invalid parentheses.");
                }
                else if (a[i] == ')')
                {
                    countCloseBrackets++;
                }
                if (countCloseBrackets > countOpenBrackets) throw new ArgumentException("Invalid parentheses.");
            }
            if (countOpenBrackets != countCloseBrackets) throw new ArgumentException("Invalid parentheses.");

        }
        public static string CalculateWithoutBrackets(String a)
        {
            var operatorsFirstPriority = new char[] { '^' };
            var operatorsSecondPriority = new char[] { '*', '/' };
            var operatorsThirdPriority = new char[] { '+', '-' };
                        
            while(a.IndexOfAny(operatorsFirstPriority) != -1)
            {
                a = MathOperations(a, operatorsFirstPriority);
            }

            while (a.IndexOfAny(operatorsSecondPriority) != -1)
            {
                a = MathOperations(a, operatorsSecondPriority);
            }

            while (a.Substring(1).IndexOfAny(operatorsThirdPriority) != -1)
            {
                a = MathOperations(a, operatorsThirdPriority);
            }

            return a;
        }
        public static string MathOperations(String a, char[] mathOperators)
        {
            double leftNum;
            double rightNum;
            
            int indexStart = 0;
            int indexEnd = a.Length - 1;
            
            Regex regex = new Regex(@"[0-9.,]");

            int indexOperator = a.Substring(1).IndexOfAny(mathOperators) + 1;
            var operatorOfExpression = a[indexOperator];
            int i;
            
            for (i = indexOperator - 1; i >= 0; i--)
            {
                string s = a[i].ToString();
                if (!regex.IsMatch(s))
                {
                    if (i == 0 && a[i] == '-') i--;
                    break;
                }
            }
            
            i++;
            indexStart = i;

            if (!double.TryParse(a.Substring(i, indexOperator - i), out leftNum))
            {
                throw new ArgumentException("Incorrect input of mathematical operators.");
            }
            
            int j;
            for (j = indexOperator + 1; j < a.Length; j++)
            {
                string s = a[j].ToString();
                if(!regex.IsMatch(s))
                {
                    if (j == indexOperator + 1 && a[j] == '-') continue;
                    break; 
                }
            }
            
            j--;
            indexEnd = j;

            if(!double.TryParse(a.Substring(indexOperator + 1, j - indexOperator), out rightNum))
            {
                throw new ArgumentException("Incorrect input of mathematical operators.");
            }            

            double result = SwitchOperations(leftNum, rightNum, operatorOfExpression);
            
            a = a.Remove(indexStart, indexEnd - indexStart + 1);
            a = a.Insert(indexStart, result.ToString());
            
            return a;
        }
        public static string OperationInBrackets(String a)
        {
            if (a.Contains("("))
            {
                var indexStart = a.IndexOf('(');
                var indexEnd = 0;

                var countOpenBrackets = 0;
                var countCloseBrackets = 0;

                var index = indexStart;

                while (index != -1) {

                    for (; index < a.Length; index++)
                    {
                        if (a[index] == '(') countOpenBrackets++;
                        if (a[index] == ')') countCloseBrackets++;
                        if (countOpenBrackets == countCloseBrackets)
                        {
                            indexEnd = index;
                            countOpenBrackets = 0;
                            countCloseBrackets = 0;
                            break;
                        }
                    }

                    var resString = a.Substring(indexStart + 1, indexEnd - indexStart - 1);
                    var result = OperationInBrackets(resString);

                    a = a.Remove(indexStart, indexEnd - indexStart + 1);
                    a = a.Insert(indexStart, result);

                    index = indexStart = a.IndexOf('(');
                }
            }            

            return CalculateWithoutBrackets(a);

        }
        public static double SwitchOperations(double a, double b, char c)
        {
            switch (c)
            {
                case '^':
                    return Math.Pow(a, b);
                case '*':
                    return a * b;
                case '/':
                    if (b == 0) throw new DivideByZeroException("Attempt division by zero.");
                    return a / b;
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                default:
                    throw new ArgumentException("Incorrect input of mathematical operators.");
            }            
        }
    }
}
