﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calculations.Extensions;

namespace CalculatorLibrary
{
    public delegate void WorkDone(double res);

    public static class CalculatorByNick
    {
        public static event WorkDone ExpressionIsCalculated;

        private const string Minus = "-";

        private static readonly List<Operator> Operators = new List<Operator>
        {
            new Operator('^', 5, (a, b) => Math.Pow(a, b)),//, (a, b) => Console.WriteLine(a + " ^ " + b)),
            new Operator('*', 6, (a, b) => a * b        ),//  (a, b) => Console.WriteLine(a + " * " + b)),
            new Operator('/', 6, (a, b) => a / b        ),//  (a, b) => Console.WriteLine(a + " / " + b)),
            new Operator('%', 6, (a, b) => a % b        ),//  (a, b) => Console.WriteLine(a + " % " + b)),
            new Operator('-', 7, (a, b) => a - b        ),//  (a, b) => Console.WriteLine(a + " - " + b)),
            new Operator('+', 7, (a, b) => a + b        ),//  (a, b) => Console.WriteLine(a + " + " + b)),

        };

        /// <summary> Example: '1 - 5 / 4 * 1.8 + (2 + 4^3)' </summary>
        public static double Calculate(string expression)
        {
            // remove empty space
            expression = string.Join("", expression.Where(x => x != ' '));

            // todo:n validate expression, prevent zero division

            var resultExpr = expression;

            while (resultExpr.Contains("("))
            {
                var lastOpenBracketIndex = resultExpr.LastIndexOf("(");
                var closeBracketIndex = resultExpr.Remove(0, lastOpenBracketIndex).IndexOf(")") + lastOpenBracketIndex;

                var exprBetweenBrackets = resultExpr.Substring(lastOpenBracketIndex + 1, closeBracketIndex - lastOpenBracketIndex - 1);

                var exprResult = CalculateExpressionWithoutBrackets(exprBetweenBrackets);

                if (ExpressionIsCalculated != null)
                {
                    ExpressionIsCalculated(exprResult);
                }
                resultExpr = resultExpr.Substring(0, lastOpenBracketIndex) + exprResult + resultExpr.Substring(closeBracketIndex + 1);
            }

            var result = CalculateExpressionWithoutBrackets(resultExpr);

            return Round(result);
        }

        private static double CalculateExpressionWithoutBrackets(string expression)
        {
            if (!ContainsOperator(expression))
            {
                return double.Parse(expression);
            }
            var op = GetOperatorWithHighestPriority(expression);
            var operatorIndex = GetOperatorIndex(expression, op.Sign);

            int leftIndex = 0;
            var leftOperand = GetLeftOperand(expression, operatorIndex, out leftIndex);

            int rightIndex = 0;
            var rightOperand = GetRightOperand(expression, operatorIndex, out rightIndex);

            var expressionResult = op.Calculate(leftOperand, rightOperand);

            if (op.PrintToConsole != null)
            {
                op.PrintToConsole(leftOperand, rightOperand);
            }

            var leftPart = expression.Substring(0, leftIndex);
            var rightPart = expression.Substring(rightIndex + 1);
            expression = leftPart + expressionResult + rightPart;

            string expressionWithoutMinusAtFront = expression;
            if (expression.StartsWith(Minus))
            {
                expressionWithoutMinusAtFront = expressionWithoutMinusAtFront.Remove(0, 1);
            }

            if (expressionWithoutMinusAtFront.Any(x => !char.IsDigit(x) && !x.IsNumberSeparator()))
            {
                expressionResult = CalculateExpressionWithoutBrackets(expression);
            }

            return expressionResult;
        }

        private static double Round(double value)
        {
            return Math.Round(value, 5, MidpointRounding.AwayFromZero);
        }

        private static bool ContainsOperator(string expression) 
        {
            if (expression.StartsWith(Minus))
            {
                expression = expression.Remove(0, 1);
            }

            return expression.Any(x => Operators.Select(o => o.Sign).Contains(x));
        }

        // order matters: from left to right
        private static double GetRightOperand(string expression, int operatorIndex, out int rightIndex)
        {
            rightIndex = operatorIndex + 1;
            var currentChar = expression[rightIndex];

            // handle negative value (skip minus sign from checking)
            if (currentChar.ToString() == Minus && expression.Length > rightIndex + 1) 
            {
                currentChar = expression[rightIndex + 1];
            }

            while (rightIndex < expression.Length && char.IsDigit(currentChar) || currentChar.IsNumberSeparator())
            {
                rightIndex++;
                if (rightIndex == expression.Length)
                {
                    break;
                }
                currentChar = expression[rightIndex];
            }

            rightIndex--;

            var rightValue = expression.Substring(operatorIndex + 1, rightIndex - operatorIndex);
            return double.Parse(rightValue);
        }

        private static int GetOperatorIndex(string expression, char op)
        {
            return expression.StartsWith(Minus)
                ? expression.Remove(0, 1).IndexOf(op.ToString()) + 1
                : expression.IndexOf(op.ToString());
        }

        // order matters: from left to right
        private static double GetLeftOperand(string expression, int operatorIndex, out int leftIndex)
        {
            leftIndex = operatorIndex - 1;
            var currentChar = expression[leftIndex];

            while (leftIndex > 0 && char.IsDigit(currentChar) || currentChar.IsNumberSeparator())
            {
                leftIndex--;
                currentChar = expression[leftIndex];
            }

            if (leftIndex != 0)
            {
                leftIndex++;
            }

            var leftValue = expression.Substring(leftIndex, operatorIndex - leftIndex);
            return double.Parse(leftValue);
        }

        // order matters: from left to right
        private static Operator GetOperatorWithHighestPriority(string expression)
        {
            if (expression.StartsWith(Minus))
            {
                expression = expression.Remove(0, 1);
            }

            var operators = expression.Where(x => Operators.Select(o => o.Sign).Contains(x))
                .ToList();

            var highestPriorityOperator = Operators.First(o => o.Sign == operators.First());

            operators.ForEach(x =>
            {
                var currentOperator = Operators.First(o => o.Sign == x);

                if (currentOperator.Priority < highestPriorityOperator.Priority)
                {
                    highestPriorityOperator = currentOperator;
                }
            });
            return highestPriorityOperator;
        }
    }
}
