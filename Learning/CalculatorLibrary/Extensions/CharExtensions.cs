﻿using System.Threading;

namespace Calculations.Extensions
{
    public static class CharExtensions
    {
        public static bool IsNumberSeparator(this char character)
        {
            var numberFormatInfo = Thread.CurrentThread.CurrentCulture.NumberFormat;
            return character.ToString() == numberFormatInfo.NumberDecimalSeparator ||
                character.ToString() == numberFormatInfo.NumberGroupSeparator;
        }
    }
}
