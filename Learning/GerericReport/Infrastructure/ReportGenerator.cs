﻿using System;
using GerericReport.Models;
using GerericReport.Reports;

namespace GerericReport.Infrastructure
{
    public static class ReportGenerator
    {
        public static IReport<T> Generate<T>(T entity) where T : Person
        {
            IReport<T> result = null;
            if (entity is Student)
            {
                result = new StudentReport().Generate(entity as Student) as IReport<T>;
            }
            else
            {
                result = new WorkerReport().Generate(entity as Worker) as IReport<T>;
            }
            return result;
        }

        /// <summary>
        /// Выводит информацию на консоль
        /// </summary>
        public static void Display<T>(IReport<T> report) where T : Person
        {
            // нужно реализовать вывод информации на консоль: для отчета по студенту и по работнику (всю информацию, которая есть в отчете)

        }
    }
}
