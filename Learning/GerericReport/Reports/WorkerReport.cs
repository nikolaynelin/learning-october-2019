﻿using GerericReport.Models;

namespace GerericReport.Reports
{
    public class WorkerReport : IReport<Worker>
    {
        /// <summary>
        /// Уровень надежности работника: зависит от количества выходных, которые он использовал
        /// </summary>
        public Level Level { get; private set; }

        public decimal Salary { get; private set; }

        public IReport<Worker> Generate(Worker worker)
        {
            Level = worker.DaysOffCount > 5 ? Level.Middle : Level.Hight;
            Salary = worker.Salary;

            return this;
        }
    }

    public enum Level
    {
        Hight = 1,

        Middle = 2,

        Low = 3,
    }
}
