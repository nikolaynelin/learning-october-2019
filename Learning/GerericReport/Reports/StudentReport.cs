﻿using GerericReport.Models;
using System;
using System.Linq;

namespace GerericReport.Reports
{
    public class StudentReport : IReport<Student>
    {
        public double AverageMark { get; private set; }

        public double MaxMark { get; private set; }

        public double MinMark { get; private set; }

        /// <summary>
        /// Количество полученных оценок
        /// </summary>
        public int MarksCount { get; private set; }

        /// <summary>
        /// Дисциплина, по которой наихудшая успеваемость (если несколько - указываются все, через запятую)
        /// </summary>
        public string WeakSubject { get; private set; }

        public IReport<Student> Generate(Student student)
        {
            var marks = student.Marks.Select(x => x.Value);

            AverageMark = marks.Average();
            MaxMark = marks.Max();
            MinMark = marks.Min();
            WeakSubject = string.Join(",", student.Marks.Where(x => x.Value == MinMark).Select(x => x.Subject));
            MarksCount = marks.Count();

            return this;
        }
    }
}
