﻿using GerericReport.Models;

namespace GerericReport.Reports
{
    public interface IReport<TEntity> where TEntity : Person
    {
        IReport<TEntity> Generate(TEntity entity);
    }
}
