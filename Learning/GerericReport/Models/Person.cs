﻿using System;

namespace GerericReport.Models
{
    public class Person : EntityBase<Guid>
    {
        public string Name { get; set; }
    }
}
