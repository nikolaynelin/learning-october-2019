﻿using EducationProcess.Models;
using EducationProcess.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EducationProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = new Student("Иван Петренко")
            {
                Activities = new List<Activity>
                {
                    new Activity
                    {
                        StartDate = new DateTime(2019, 11, 21, 10, 0, 0),
                        // todo:n set time in hours and minutes
                        Duration = new ActivityDuration(2, 30),
                        Type = ActivityType.Practical,
                    },
                    new Activity
                    {
                        StartDate = new DateTime(2019, 11, 22, 10, 30, 0),
                        Duration = new ActivityDuration(1, 30),
                        Type = ActivityType.Practical,
                    },
                    new Activity
                    {
                        StartDate = new DateTime(2019, 11, 23, 10, 30, 0),
                        Duration = new ActivityDuration(1, 30),
                        Type = ActivityType.Practical,
                    },
                }
            };

            Console.OutputEncoding = System.Text.Encoding.Unicode;

            var totalPracticalTime = student[ActivityType.Practical];
            System.Diagnostics.Debug.Assert(totalPracticalTime == "дней: 0; часов: 5; минут: 30");
            //WriteLine($"студент: {student.FullName}; общая продолжительность практических занятий: {totalPracticalTime}");
            //WriteLine();

            var date = new DateTime(2019, 11, 22, 10, 0, 0);
            List<Activity> activities = student[date].ToList();

            System.Diagnostics.Debug.Assert(activities.Count == 1 && activities.First().StartDate.Day == 22);

            //WriteLine($"занятия студента {student.FullName} за {date}:");

            //foreach (var a in activities)
            //{
            //    WriteLine(a.Type + "; продолжительность: " + a.Duration);
            //}
        }

        private static void WriteLine(string text)
        {
            System.Console.WriteLine(text);
        }

        private static void WriteLine()
        {
            System.Console.WriteLine();
        }
    }
}
