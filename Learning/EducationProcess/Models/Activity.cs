﻿using EducationProcess.Models.Enums;
using System;

namespace EducationProcess.Models
{
    public class Activity
    {
        public ActivityType Type { get; set; }

        public DateTime StartDate { get; set; }

        public ActivityDuration Duration { get; set; }
    }
}
