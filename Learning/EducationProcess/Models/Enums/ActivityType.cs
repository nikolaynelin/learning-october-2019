﻿namespace EducationProcess.Models.Enums
{
    public enum ActivityType
    {
        Practical,

        Theory,
    }
}
