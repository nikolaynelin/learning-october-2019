﻿using EducationProcess.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EducationProcess.Models
{
    public class Student
    {
        public Student(string fullName)
        {
            // todo:n check for digits, etc
            if (string.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException(nameof(fullName));
            }
            FullName = fullName;
        }

        public string FullName { get; }

        // todo:n incapsulate
        public List<Activity> Activities = new List<Activity>();

        /// <summary> Total activities durations by type (formatted text) </summary>
        public string this[ActivityType type]
        {
            get
            {
                ActivityDuration activitiDurations = new ActivityDuration(0);
                foreach (var item in Activities.Where(x => x.Type == type))
                {
                    activitiDurations += item.Duration;
                }
                return activitiDurations.Duration;
            }
        }

        /// <summary> All activities by date </summary>
        public IEnumerable<Activity> this[DateTime date] =>
            Activities.Where(x => x.StartDate >= date && x.StartDate.Date == date.Date);

        public IEnumerable<Activity> this[DateTime date, ActivityType type]
            => this[date].Where(x => x.Type == type);
    }
}
