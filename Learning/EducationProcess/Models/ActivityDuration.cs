﻿using System;

namespace EducationProcess.Models
{
    public class ActivityDuration
    {
        private TimeSpan _duration;

        public string Duration => $"дней: {_duration.Days}; часов: {_duration.Hours}; минут: {_duration.Minutes}";

        public int TotalMinutes
        {
            get
            {
                return (int)_duration.TotalMinutes;
            }
        }

        public ActivityDuration(int minutes)
        {
            _duration = TimeSpan.FromMinutes(minutes);
        }

        public ActivityDuration(int hours, int minutes)
        {
            _duration = TimeSpan.FromMinutes(hours * 60 + minutes);
        }

        public static ActivityDuration operator +(ActivityDuration a, ActivityDuration b)
        {
            var totalMinutes = (int)TimeSpan.FromMinutes(a.TotalMinutes + b.TotalMinutes).TotalMinutes;
            return new ActivityDuration(totalMinutes);
        }

        public override string ToString()
        {
            return Duration;
        }
    }
}
