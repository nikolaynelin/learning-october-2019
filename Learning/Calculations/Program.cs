﻿using CalculatorLibrary;
using CollectionsEx;
using System;
using System.Diagnostics;
using System.Threading;

namespace Calculations
{
    class Program
    {

        private static double F(string e)
        {
            return 3.1;
        }

        private static void Wait()
        {
            //Thread.Sleep(2000);
        }

        static void Main(string[] args)
        {

            var list = new LinkedListEx<int>();
            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddLast(4);
            list.AddLast(5);

            return;

            var arr = new int[5];

            for(var i = 0; i < 6; i++)
            {
                arr[i] = i;
                Console.Write($"i: {i}; ");
            }

            return;
            //Calculator.ExpressionIsCalculated += PrintResultToConsole;
            //Calculator.ExpressionIsCalculated += SumAllResult;

            var result = CalculatorByNick.Calculate("1 - 5 / 4 * 1.8 + 2");
            Debug.Assert(result == 0.75);
            Console.WriteLine("1");
            Wait();

            result = CalculatorByNick.Calculate("-1 - 5 / 4 * 1.8 + 2");
            Debug.Assert(result == -1.25);
            Console.WriteLine("2");

            Wait();

            result = CalculatorByNick.Calculate("-1 - 5 / 4 * 1.8 + 2^3");
            Debug.Assert(result == 4.75);
            Console.WriteLine("3");

            Wait();

            result = CalculatorByNick.Calculate("2^3^2");
            Debug.Assert(result == 64);
            Wait();

            //   Calculator.ExpressionIsCalculated -= PrintResultToConsole;

            result = CalculatorByNick.Calculate("(1 / (10 + 5) - 4) * (7 / (4 - 2))");
            Debug.Assert(result == -13.766666666666657);
            Wait();

            result = CalculatorByNick.Calculate("1 - 5 / 4 * 1.8 + (2 + 4^3)");
            Debug.Assert(result == 64.75);
            Wait();

            result = CalculatorByNick.Calculate("-3-1");
            Debug.Assert(result == -4);
            Wait();

            result = CalculatorByNick.Calculate("-5-(3-5)");
            Debug.Assert(result == -3);

            result = CalculatorByNick.Calculate("(1-5)");
            Debug.Assert(result == -4);

            result = CalculatorByNick.Calculate("((((-1) - (((5) / 4) * (1.8))) + (2)))");
            Debug.Assert(result == -1.25);

            result = CalculatorByNick.Calculate("-(1-(-5))");
            Debug.Assert(result == -6);

            Console.WriteLine("Success!");
        }

        private static void PrintResultToConsole(double res)
        {
            Console.WriteLine(res);
        }

        private static double total = 0;

        private static void SumAllResult(double res)
        {
            total += res;
            Console.WriteLine("total: " + total);
        }
    }
}
