﻿using Calculator;
using CalculatorLibrary;
using NUnit.Framework;
using System;

namespace CalculatorTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        [TestCase("1-4", -3)]
        [TestCase("1 - 5 / 4 * 1.8 + 2", 0.75)]
        [TestCase("-1 - 5 / 4 * 1.8 + 2", -1.25)]
        [TestCase("-1 - 5 / 4 * 1.8 + 2^3", 4.75)]
        [TestCase("2^3^2", 64)]
        [TestCase("(1 / (10 + 5) - 4) * (7 / (4 - 2))", -13.76667)]
        [TestCase("1 - 5 / 4 * 1.8 + (2 + 4^3)", 64.75)]
        [TestCase("-3-1", -4)]
        [TestCase("-5-(3-5)", -3)]
        [TestCase("(1-5)", -4)]
        [TestCase("((((-1) - (((5) / 4) * (1.8))) + (2)))", -1.25)]
        [TestCase("-(1-(-5))", -6)]
        [TestCase("2 ^ 3 * 5.6 ^ 5", 44058.54208)]
        [TestCase("2.5 ^ 5 / 2", 48.82813)]
        [TestCase("1 - 5 /4 * 1.8", -1.25)]
        [TestCase("1 - 5 *4 / 1.8", -10.11111)]
        [TestCase("1 - 5 /4 * -1.8", 3.25)]
        [TestCase("-1 * -3", 3)]
        [TestCase("1 - 5 / 4 * 1.8 + (2 + 4)", 4.75)]
        [TestCase("(1 + 2) * ((2 * 5 - 2) + (8 / 4 * 3) / 2 + 5) - 5 * (-1)", 53)]
        [TestCase("-3 - 1", -4)]
        [TestCase("6 - 3 * -1", 9)]
        [TestCase("-5-(3-5)", -3)]
        [TestCase("(-1 - 5)", -6)]
        public void Calculate_ShouldWorkCorrectly(string expression, double expected)
        {
            // act
            var actual = CalculatorByNick.Calculate(expression);
            var actual2 = CalculatorByDima.Calculate(expression);

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual2);
        }

        [Test]
        public void Calculate_ShouldThrowException_WhenZeroDevision()
        {
            Assert.Throws<DivideByZeroException>(() => CalculatorByDima.Calculate("1/0"));
        }
    }
}
