﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contacts
{
    public partial class Details : Form
    {
        public Contact Contact { get; private set; }

        public Details()
        {
            Contact = new Contact();
            InitializeComponent();
            this.CenterToParent();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            Contact.Name = textBox_name.Text;
            Hide();
        }
    }
}
