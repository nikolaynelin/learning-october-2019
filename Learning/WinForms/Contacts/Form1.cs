﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contacts
{
    public partial class Form1 : Form
    {
        public static List<Contact> ContactList = new List<Contact> { 
        new Contact
        {
            Name = "Nikolay Nelin",
            Phone = "+380660510938",
            BirthDate = new DateTime(1992, 5, 22),
        }
        };

        public Form1()
        {
            InitializeComponent();

            RefreshDataGridView();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            var detailsForm = new Details();
            var result = detailsForm.ShowDialog(this);

            ContactList.Add(detailsForm.Contact);

            RefreshDataGridView();
        }

        private void RefreshDataGridView(List<Contact> list = null)
        {
            dataGridView.DataSource = null;
            dataGridView.DataSource = list ?? ContactList;
            dataGridView.Refresh();
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedCells.Count < 1)
            {
                return;
            }
            ContactList.RemoveAt(dataGridView.SelectedCells[0].RowIndex);
            RefreshDataGridView();
        }

        private void textBox_filter_TextChanged(object sender, EventArgs e)
        {
            var filtered = ContactList.Where(x => x.Name.ToUpper().Contains(textBox_filter.Text.ToUpper()))
                .ToList();
            RefreshDataGridView(filtered);
        }
    }

    public class Contact
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? Age => BirthDate.HasValue ? (int?)(DateTime.Now - BirthDate).Value.TotalDays / 365 : null;
    }
}
