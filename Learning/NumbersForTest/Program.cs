﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumbersForTest
{
    class Program
    {
        private static List<string> _numbers = new List<string> { };
        private static HashSet<int> _existing = new HashSet<int>();
        private static Random _rand = new Random();

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            for (var i = 1; i < 11; i++)
            {
                _numbers.Add(i.ToString());
            }

            Console.WriteLine("Осторожно, не закрывайте программу до завершения выбора номеров вопросов!!!\n");

            bool duplicate = false;

            while (_numbers.Count > 0)
            {
                int parsedNumber = 0;
                bool valid = false;

                do
                {
                    duplicate = false;

                    Console.Write($"Загадайте число от 1 до 100: ");
                    var number = Console.ReadLine();
                    
                    valid = int.TryParse(number, out parsedNumber);
                    if (valid && _existing.Contains(parsedNumber))
                    {
                        Console.WriteLine("Это число уже загадывали! Загадайте другое.\n");
                        duplicate = true;
                    }
                    _existing.Add(parsedNumber);

                } while (!valid || parsedNumber < 1 || parsedNumber > 100 || duplicate);


                var index = _rand.Next(0, _numbers.Count - 1);
                var numberOfQuestion = _numbers[index];
                _numbers.RemoveAt(index);

                Console.WriteLine($"Номер вашего вопроса {numberOfQuestion}.\n");
            }

            Console.WriteLine($"Все номера вопросов были использованы.");
            Console.ReadLine();
        }
    }
}
