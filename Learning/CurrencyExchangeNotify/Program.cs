﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace CurrencyExchangeNotify
{
    class Program
    {
        private static Dictionary<string, Func<string, decimal>> _methodsByUrls = new Dictionary<string, Func<string, decimal>>
        {
            { "https://minfin.com.ua/currency/kharkov/", (str) => ParseMinfin(str) },
            { "https://www.prostobank.ua/spravochniki/kursy_valyut", (str) => ParseProstobank(str) },
        };

        private static decimal _previousAverageRate;

        private static readonly int Timeout = 1000;

        private const decimal MinDifference = 0.1m;

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            // todo:n IDisposable

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            var newRates = new HashSet<decimal>();

            while (true)
            {
                newRates.Clear();

                try
                {
                    foreach (var parseMethodByWebSite in _methodsByUrls)
                    {
                        string str = null;
                        using (var client = new WebClient())
                        {
                            client.Headers.Add("user-agent", "Only a test!");

                            client.Encoding = Encoding.UTF8;
                            str = client.DownloadString(parseMethodByWebSite.Key);
                        }

                        var value = parseMethodByWebSite.Value(str);
                        newRates.Add(value);
                    }
                }
                catch(Exception ex)
                {
                    PrintToFile(ex.Message);
                    PrintToConsole(ex.Message);
                    Beep();
                }

                var newAverageRate = newRates.Average();

                var notifyMessage = $"{DateTime.Now:HH:mm:ss} => {newAverageRate:#.####}";

                if (Math.Abs(newAverageRate - _previousAverageRate) >= MinDifference)
                {
                    Beep();

                    PrintToFile(notifyMessage);

                    _previousAverageRate = newAverageRate;
                }

                PrintToConsole(notifyMessage);

                Thread.Sleep(Timeout);
            }
        }

        private static void PrintToConsole(string message)
        {
            Console.WriteLine(message);
        }

        private static void PrintToFile(string message)
        {
            using (var writer = File.AppendText(@"../../data.txt"))
            {
                writer.WriteLine(message);
            }
        }

        private static decimal ParseProstobank(string str)
        {
            var index = str.IndexOf("table-currency");
            str = str.Remove(0, index);

            index = str.IndexOf("доллар США");
            str = str.Remove(0, index);

            var term = "col col-currency-rate";
            index = str.IndexOf(term);
            str = str.Remove(0, index+term.Length);

            var lastTerm = @"col col-currency-rate""><p>";
            index = str.IndexOf(lastTerm);
            index = index == 0 ? index + term.Length : index;

            str = str.Remove(0, index + lastTerm.Length);

            str = str.Remove(str.IndexOf("</p>"));

            decimal value = decimal.Parse(str.Replace(",", "."));
            return value;
        }

        private static decimal ParseMinfin(string str)
        {
            var index = str.IndexOf("mfcur-table-lg-currency");
            str = str.Remove(0, index);

            index = str.IndexOf("ДОЛЛАР");
            str = str.Remove(0, index);

            var lastTerm = @"mfcur-nbu-full-wrap"">";
            index = str.IndexOf(lastTerm);
            str = str.Remove(0, index + lastTerm.Length);

            str = str.Remove(str.IndexOf("<span"));

            decimal value = decimal.Parse(str.Replace(",", "."));
            return value;
        }

        private static void Beep()
        {
            const int beepDuration = 500;
            const int beepTimeout = 100;

            new int[] { 1000, 900, 800 }.ForEach(tone =>
            {
                Console.Beep(tone, beepDuration);
                Thread.Sleep(beepTimeout);
            });
        }
    }
}
