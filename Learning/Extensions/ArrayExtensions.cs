﻿using System;
using System.Collections.Generic;

namespace CurrencyExchangeNotify
{
    public static class ArrayExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }
    }
}
