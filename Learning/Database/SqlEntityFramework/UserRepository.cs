﻿using DomainEntities;
using System;
using System.Data.Entity;
using System.Linq;

namespace SqlEntityFramework
{
    public class UserRepository : IEntityFrameworkUserRepository
    {
        private readonly UserManagerDbContext _context;

        public UserRepository(UserManagerDbContext context)
        {
            _context = context;
        }

        public User Add(User entity)
        {
            SetCreatedOn(entity);
            _context.Users.Add(entity);
            return entity;
        }

        public User Get(object id)
        {
            return _context.Users.Find(id);
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users;
        }

        public void Remove(object id)
        {
            var entityToDelete = _context.Users.Find(id);

            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _context.Users.Attach(entityToDelete);
            }
            _context.Users.Remove(entityToDelete);
        }

        public User Update(User entity)
        {
            SetModifiedOn(entity);
            _context.Users.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private static void SetModifiedOn(User entity)
        {
            entity.UpdatedOn = DateTime.Now;
        }

        private static void SetCreatedOn(User entity)
        {
            entity.CreatedOn = DateTime.Now;
        }
    }
}