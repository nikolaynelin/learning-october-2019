﻿using DomainEntities;
using Repositories;
using System;

namespace SqlEntityFramework
{
    public interface IEntityFrameworkRepository<T, TId> : IRepository<T, TId>, IDisposable where T : EntityBase<TId>
    {
        void SaveChanges();
    }
}