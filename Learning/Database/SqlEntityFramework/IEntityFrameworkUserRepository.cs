﻿using DomainEntities;
using Repositories;
using System;

namespace SqlEntityFramework
{
    public interface IEntityFrameworkUserRepository : IUserRepository, IEntityFrameworkRepository<User, Guid>
    {
    }
}