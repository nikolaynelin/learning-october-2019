﻿using DomainEntities;
using System.Data.Entity;

namespace SqlEntityFramework
{
    public class UserManagerDbContext : DbContext
    {
        public UserManagerDbContext() : base("Data Source=NICK-LAPTOP\\SQLEXPRESS;Initial Catalog=users_ef;Integrated Security=SSPI;User ID=local;Password=111")
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
