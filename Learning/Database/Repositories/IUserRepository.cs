﻿using DomainEntities;
using System;

namespace Repositories
{
    public interface IUserRepository : IRepository<User, Guid>
    {
    }
}
