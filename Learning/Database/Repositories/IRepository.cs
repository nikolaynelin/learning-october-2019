﻿using DomainEntities;
using System.Linq;

namespace Repositories
{
    public interface IRepository<T, TId> where T : EntityBase<TId>
    {
        T Get(object id);

        IQueryable<T> GetAll();

        void Remove(object id);

        T Add(T entity);

        T Update(T entity);
    }
}
