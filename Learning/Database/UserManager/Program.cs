﻿using DomainEntities;
using SqlOleDb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UserManager
{
    class Program
    {
        private static string _connStr;

        private static readonly Dictionary<string, Action<string>> _commands = new Dictionary<string, Action<string>>
        {
            { "add", AddUser },
            { "del", RemoveUser },
        };

        static void Main(string[] args)
        {
            _connStr = ConfigurationManager.ConnectionStrings["DefaultDbConnection"].ConnectionString;

            PrintList();

            var availableCommandsMessage = $"\nAvailable commands: {string.Join(", ", _commands.Keys)} ([name] param for both is required)\n";
            Console.WriteLine(availableCommandsMessage);

            var userInput = "";
            while (userInput.ToLower() != "exit")
            {
                userInput = Console.ReadLine();

                if (!userInput.Trim().Contains(" "))
                {
                    Console.WriteLine(availableCommandsMessage);
                    continue;
                }

                var command = userInput.Split()[0].ToLower();
                var name = userInput.Split()[1];

                if (_commands.ContainsKey(command))
                {
                    _commands[command](name);
                    PrintList();
                }
            }
        }

        private static void RemoveUser(string name)
        {
            var repository = new UserRepository(_connStr);
            repository.RemoveByName(name);
        }

        private static void AddUser(string name)
        {
            var repository = new UserRepository(_connStr);

            var newUser = new User
            {
                Name = name
            };

            repository.Add(newUser);
        }

        private static void PrintList()
        {
            var repository = new UserRepository(_connStr);

            var users = repository.GetAll()
               .ToList();

            Console.WriteLine($"\nUsers count: {users.Count}\n");

            users.ForEach((u) =>
            {
                Console.WriteLine(u.ToString());
            });

            Console.WriteLine();
        }
    }
}
