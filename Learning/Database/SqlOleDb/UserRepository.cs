﻿using DomainEntities;
using Repositories;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace SqlOleDb
{
    // todo:n добавить базовый репозиторий, где будут все методы, а здесь их вызывать, задавая только CommandText
    public class UserRepository : IUserRepository
    {
        private const string TableName = "Users";

        // todo:n переместить в конфиг файл
        private readonly string ConnStr;
        
        public UserRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException(nameof(connectionString));
            }

            ConnStr = connectionString;
        }

        public User Add(User entity)
        {
            using (OleDbConnection cnn = new OleDbConnection(ConnStr))
            {
                var cmd = cnn.CreateCommand();

                SetCreatedOn(entity);

                cmd.CommandText = $"insert into {TableName} (Id, Name, CreatedOn, UpdatedOn)" +
                    $" values('{entity.Id}','{entity.Name}','{entity.CreatedOn}', '{entity.UpdatedOn}')";

                cnn.Open();

                var result = cmd.ExecuteNonQuery();

                cnn.Close();

                if (result == 0)
                {
                    throw new Exception("0 rows affected");
                }

                return Get(entity.Id);
            }
        }

        public void RemoveByName(string name)
        {
            using (OleDbConnection cnn = new OleDbConnection(ConnStr))
            {
                var cmd = cnn.CreateCommand();

                cmd.CommandText = $"delete from {TableName} where [Name]='{name}'";

                cnn.Open();

                var result = cmd.ExecuteNonQuery();

                cnn.Close();

                if (result == 0)
                {
                    throw new Exception("0 rows affected");
                }
            }
        }

        private static void SetModifiedOn(User entity)
        {
            entity.UpdatedOn = DateTime.Now;
        }

        private static void SetCreatedOn(User entity)
        {
            entity.CreatedOn = DateTime.Now;
        }

        public User Get(object id)
        {
            using (OleDbConnection cnn = new OleDbConnection(ConnStr))
            {
                var cmd = cnn.CreateCommand();

                cmd.CommandText = $"select top 1 * from {TableName} where Id='{id}'";

                cnn.Open();

                var reader = cmd.ExecuteReader();

                if (!reader.Read())
                {
                    throw new Exception("can't read");
                }

                var result = new User();

                result.Id = reader.GetGuid(0);
                result.Name = reader.GetString(1);
                result.CreatedOn = reader.GetDateTime(2);
                result.UpdatedOn = reader.GetDateTime(3);

                cnn.Close();

                return result;
            }
        }

        public IQueryable<User> GetAll()
        {
            var result = new List<User>();

            using (OleDbConnection cnn = new OleDbConnection(ConnStr))
            {
                var cmd = cnn.CreateCommand();

                cmd.CommandText = $"select * from {TableName}";

                cnn.Open();

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var current = new User();

                    current.Id = reader.GetGuid(0);
                    current.Name = reader.GetString(1);
                    current.CreatedOn = reader.GetDateTime(2);
                    current.UpdatedOn = reader.GetDateTime(3);

                    result.Add(current);
                }

                cnn.Close();

                return result.OrderByDescending(x => x.CreatedOn)
                    .AsQueryable();
            }
        }

        public void Remove(object id)
        {
            throw new NotImplementedException();
        }

        public User Update(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
