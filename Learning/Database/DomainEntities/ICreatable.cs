﻿using System;

namespace DomainEntities
{
    /// <summary>
    /// Set value before saving into database
    /// </summary>
    public interface ICreatable
    {
        DateTime? CreatedOn { get; set; }
    }
}
