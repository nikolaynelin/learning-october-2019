﻿namespace DomainEntities
{
    public interface IEntity : IEditable, ICreatable
    {
    }
}
