﻿using System;

namespace DomainEntities
{
    public interface IEditable
    {
        DateTime? UpdatedOn { get; set; }
    }
}
