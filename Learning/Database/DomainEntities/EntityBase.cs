﻿using System;

namespace DomainEntities
{
    public abstract class EntityBase<TId> : IEntity
    {
        public TId Id { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
