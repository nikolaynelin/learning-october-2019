﻿using System;

namespace DomainEntities
{
    public class User : EntityBase<Guid>
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        public string Name { get; set; }

        public override string ToString()
           => $"Created On: {CreatedOn:yyyy-MM-dd}; Name: {Name}";
    }
}
