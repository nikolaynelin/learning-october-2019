﻿using System.Collections.Generic;
using System.Linq;
using WorkerManager.Extensions;
using WorkerManager.Models;

namespace WorkerManager
{
    class Program
    {
        static void Main(string[] args)
        {
            var workers = GetWorkers();

            DisplayInfo(workers);

            DoWork(workers);
        }

        private static void DoWork(List<IWorker> workers)
        {
            foreach (var worker in workers)
            {
                var workInfo = worker.DoWork();
                WriteLine(workInfo);
            }
        }

        private static void WriteLine(string text)
        {
            System.Console.WriteLine(text);
        }

        private static void WriteLine()
        {
            System.Console.WriteLine();
        }

        private static List<IWorker> GetWorkers()
        {
            var workers = new List<IWorker>
            {
                new FactoryWorker("Vasily", 1000, "I'm very experiensed factory worker (10 years)."),
                new FactoryWorker("Sergey", 5000),
                new Director("Peter", 3700),
                new Driver("Alexey", 10000),
                new Writer("Peterson", 51000)
                {
                    Summary = "I'm super writer",
                },
            };
            return workers;
        }

        private static void DisplayInfo(List<IWorker> workers)
        {
            foreach (var workersGroup in workers.GroupBy(x => x.Position))
            {
                WriteLine($"{workersGroup.Key.GetWorkerGroupName()}: {workersGroup.Count()}");
                WriteLine();

                foreach (var worker in workersGroup)
                {
                    WriteLine(worker.GetInfo());
                }

                WriteLine();
                WriteLine();
            }
        }
    }
}
