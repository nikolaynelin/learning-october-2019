﻿using WorkerManager.Models.Enums;

namespace WorkerManager.Models
{
    public interface IWorker
    {
        string Summary { get; set; }
        Position Position { get; }
        decimal Salary { get; }
        string DoWork();
        string FullName { get; set; }
        Experiense Experiense { get; set; }
        string GetInfo();
    }

    public abstract class WorkerBase : IWorker
    {
        public WorkerBase(string fullName, decimal salary)
        {
            FullName = fullName;
            Salary = salary;
        }

        public decimal Salary { get; }

        public string FullName { get; set; }

        public virtual string Summary { get; set; }

        public Experiense Experiense { get; set; }

        public abstract Position Position { get; }

        /// <summary> Display message what worker do </summary>
        public abstract string DoWork();

        public string GetInfo()
        {
            return $"name: {FullName}; position: {Position}; salary: {Salary.ToString("C")}; " +
                   $"{(string.IsNullOrEmpty(Summary) ? "" : $"summary: {Summary}")}";
        }

        public override string ToString()
        {
            return GetInfo();
        }
    }
}
