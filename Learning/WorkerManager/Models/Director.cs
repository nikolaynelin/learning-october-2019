﻿using System;
using WorkerManager.Models.Enums;

namespace WorkerManager.Models
{
    public class Director : WorkerBase
    {
        public Director(string fullName, decimal salary) : base(fullName, salary)
        {
        }

        public override Position Position
        {
            get
            {
                return Position.Director;
            }
        }

        public override string DoWork()
        {
            return "Give wisdom directives";
        }
    }
}
