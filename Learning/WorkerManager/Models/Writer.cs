﻿using System;
using WorkerManager.Models.Enums;

namespace WorkerManager.Models
{
    public class Writer : WorkerBase
    {
        public Writer(string fullName, decimal salary) : base(fullName, salary)
        {
        }

        public override Position Position
        {
            get
            {
                return Position.Writer;
            }
        }

        public override string DoWork()
        {
            return "Write book";
        }
    }
}
