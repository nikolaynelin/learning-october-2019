﻿using System;
using WorkerManager.Models.Enums;

namespace WorkerManager.Models
{
    public class FactoryWorker : WorkerBase
    {
        private string _summary;

        public FactoryWorker(string fullName, decimal salary) : base(fullName, salary)
        {
        }

        public FactoryWorker(string fullName, decimal salary, string summary) : this(fullName, salary)
        {
            _summary = summary;
        }

        public override Position Position
        {
            get
            {
                return Position.FactoryWorker;
            }
        }

        public override string Summary
        {
            get
            {
                return _summary;
            }
        }

        public override string DoWork()
        {
            // do what you should...
            return "Work at the machine";
        }
    }
}
