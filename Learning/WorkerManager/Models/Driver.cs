﻿using System;
using WorkerManager.Models.Enums;

namespace WorkerManager.Models
{
    public class Driver : WorkerBase
    {
        public Driver(string fullName, decimal salary) : base(fullName, salary)
        {
        }

        public override Position Position
        {
            get
            {
                return Position.Driver;
            }
        }

        public override string DoWork()
        {
            return "Drive a car";
        }
    }
}
