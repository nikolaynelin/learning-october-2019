﻿namespace WorkerManager.Models.Enums
{
    public enum Position
    {
        //todo:n figure out
        //[Display(Name = "Factory worker")]
        FactoryWorker = 1,

        Director = 2,

        Driver = 3,

        Writer = 4,
    }
}
