﻿using System;
using WorkerManager.Models.Enums;

namespace WorkerManager.Extensions
{
    public static class PositionExtensions
    {
        public static string GetWorkerGroupName(this Position position)
        {
            switch (position)
            {
                case Position.Director:
                    return "Directors";
                case Position.FactoryWorker:
                    return "Factory workers";
                case Position.Writer:
                case Position.Driver:
                    return position.ToString();
                default:
                    throw new ArgumentOutOfRangeException(nameof(position));

                    Position.Director.GetWorkerGroupName();
            }
        }
    }
}
