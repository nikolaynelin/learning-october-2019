﻿using CollectionsEx;
using System;

namespace CollectionTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            double temp = 23.3;
            float temp2 = 332.2f;

            // автоматически по выходу из блока вызовется метод Dispose,
            // поскольку класс ListEx<T> реализовывает интерфейс IDisposable
            using (var list = new ListEx<int>())
            {
                list.Add(1);
                list.Add(2);
                list.Add(3);

                foreach (int item in list)
                {
                    Console.WriteLine($"{item}");
                }
            }

            // ручной вызов метода Dispose
            var list2 = new ListEx<string>();
            try
            {
                list2.Add("dsdfdfs");
            }
            finally
            {
                list2.Dispose();
            }

            var person = new Person();
            person.FriendsCount = null;
            person.FriendsCount = 100;

            //    person.CreatedOn = DateTime.Now;
            Save(person);

            person.FriendsCount++;
            //   person.ModifiedOn = DateTime.Now;
            Save(person);


            person.FriendsCount = 0;
            //  person.ModifiedOn = DateTime.Now;

            Save(person);
        }

        private static void Save<T>(T item) where T : class
        {
            SetCreatedOn(item);
            SetModifiedOn(item);
        }

        private static void SetModifiedOn<T>(T item) where T : class
        {
            var editable = item as IEditable;
            if (editable != null)
            {
                editable.ModifiedOn = DateTime.Now;
            }
        }

        private static void SetCreatedOn<T>(T item) where T : class
        {
            var creatable = item as ICreatable;
            if (creatable != null && creatable.CreatedOn == DateTime.MinValue)
            {
                creatable.CreatedOn = DateTime.Now;
            }
        }
    }

    public class Person : ICreatable, IEditable
    {
        public string Name { get; set; }
        public int? FriendsCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }

    /// <summary>
    /// Set value before saving into database
    /// </summary>
    public interface ICreatable
    {
        DateTime CreatedOn { get; set; }
    }

    public interface IEditable
    {
        DateTime? ModifiedOn { get; set; }
    }
}
