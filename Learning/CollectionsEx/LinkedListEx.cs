﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsEx
{
    public class Node<T>
    {
        public T Value { get; set; }
        public Node<T> Prev { get; set; }
        public Node<T> Next { get; set; }
    }
    public class LinkedListEx<T>
    {
        private Node<T> _first;

        public void AddLast(Node<T> node)
        {
            if (_first == null)
            {
                _first = node;
            }
            else
            {
                var last = _first;
                while (last.Next != null)
                {
                    last = last.Next;
                }
                last.Next = node;
                node.Prev = last;
            }
        }

        public void AddLast(T value)
        {
            var newNode = new Node<T> { Value = value };

            if (_first == null)
            {
                _first = newNode;
            }
            else
            {
                var last = _first;
                while (last.Next != null)
                {
                    last = last.Next;
                }
                last.Next = newNode;
                newNode.Prev = last;
            }
        }
    }
}
