﻿using System.Collections;
using System.Collections.Generic;

namespace CollectionsEx
{
    public class ListEx<T> : IEnumerable, IEnumerator<T>
    {
        private const int InitialCapacity = 4;
        private int _size = 0;

        private T[] _data = new T[InitialCapacity];
        private int _currentIndex;

        public void Add(T item)
        {
            if (_size == _data.Length)
            {
                EnsureCapacity();
            }
            _data[_currentIndex] = item;
            _currentIndex++;
            _size++;
        }

        public int Count => _size;

        private void EnsureCapacity()
        {
        }

        public IEnumerator GetEnumerator()
        {
            return this;
        }

        private int _currentEnumeratorIndex = 0;

        public T Current => _data[_currentEnumeratorIndex];

        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            if (_currentEnumeratorIndex == _size)
            {
                return false;
            }
            _currentEnumeratorIndex++;
            return true;
        }

        public void Reset()
        {
            _currentEnumeratorIndex = 0;
        }

        public void Dispose()
        {
            // close database connection
        }
    }
}
