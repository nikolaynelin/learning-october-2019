using System;
using System.Collections.Generic;
using System.Linq;
using DomainEntities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using SqlEntityFramework;
using Telegram.Bot;

namespace UserManager.TelegramBot
{
    public class Program
    {
        private static TelegramBotClient _telegramBot;

        private static readonly Dictionary<string, Action<string>> _commands = new Dictionary<string, Action<string>>
        {
            { "/add", AddUser },
            { "/delete", RemoveUser },
            { "/update", UpdateUser },
        };

        public static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            InitConfig();

            _telegramBot.OnMessage += Client_OnMessage;
            _telegramBot.StartReceiving();

            host.Run();
        }

        private static void InitConfig()
        {
            _telegramBot = new TelegramBotClient(Startup.Configuration.GetValue<string>("TelegramBotId"));
        }

        private static void Client_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            var text = e.Message.Text;

            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            if (!text.Trim().Contains(" "))
            {
                _telegramBot.SendTextMessageAsync(e.Message.Chat, $"Invalid command name or parameters");
                return;
            }

            var command = text.Split()[0].ToLower();
            var name = string.Join(" ", text.Split().Skip(1));

            if (_commands.ContainsKey(command))
            {
                try
                {
                    _commands[command](name);
                }
                catch (Exception ex)
                {
                    _telegramBot.SendTextMessageAsync(e.Message.Chat, $"Error: {ex.Message}");
                }
                PrintList(e.Message.Chat);
            }
        }

        private static void RemoveUser(string name)
        {
            DbActionWrapper(repository =>
            {
                var userToRemove = repository.GetAll().First(x => x.Name == name);
                repository.Remove(userToRemove.Id);
            });
        }

        private static void AddUser(string name)
        {
            DbActionWrapper(repository =>
            {
                var newUser = new User
                {
                    Name = name
                };

                repository.Add(newUser);
            });
        }

        private static void UpdateUser(string name)
        {
            DbActionWrapper(repository =>
            {
                var user = repository.GetAll().First(x => x.Name == name);
                repository.Update(user);
            });
        }

        private static void DbActionWrapper(Action<UserRepository> action)
        {
            using (var repository = new UserRepository(new UserManagerDbContext()))
            {
                action(repository);
                repository.SaveChanges();
            }
        }

        private static void PrintList(Telegram.Bot.Types.Chat chat)
        {
            List<User> users = null;
            using (var repository = new UserRepository(new UserManagerDbContext()))
            {
                users = repository.GetAll()
                   .ToList();
            }

            var message = $"{Environment.NewLine}Users count: {users.Count}{Environment.NewLine}";

            var index = 0;
            users.ForEach(user =>
            {
                message += $"{Environment.NewLine}{++index}. {user.ToString()}";
            });

            message += Environment.NewLine;

            _telegramBot.SendTextMessageAsync(chat, message);
        }
    }
}
